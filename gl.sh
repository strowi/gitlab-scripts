#!/bin/bash

SCRIPTDIR="$(dirname "$0")"

# shellcheck source=includes/common.sh
source "$SCRIPTDIR/includes/common.sh"
# shellcheck source=includes/registry.sh
source "$SCRIPTDIR/includes/registry.sh"
# shellcheck source=includes/branches.sh
source "$SCRIPTDIR/includes/branches.sh"
# shellcheck source=includes/integrations-jira.sh
source "$SCRIPTDIR/includes/integrations-jira.sh"
# shellcheck source=includes/clone.sh
source "$SCRIPTDIR/includes/clone.sh"
# shellcheck source=includes/pipeline_check.sh
source "$SCRIPTDIR/includes/pipeline_check.sh"

echo "DRY_RUN: ${DRY_RUN}"

if [ "x${CLONE}" == "xtrue" ]; then
  echo ""
  echo "Cloning Repos"
  repos_clone && echo "Finished successfully" || echo "ERROR cloning repos"
else
  echo "NOT cloning repos"
fi

if [ "x${BRANCH}" == "xtrue" ]; then
  echo ""
  echo "Working branches"
  repo_branches_clean_purged && echo "Finished successfully" || echo "ERROR cleaning merged branches"
else
  echo "NOT cleaning merged branches"
fi

if [ "x${REGISTRY}" == "xtrue" ]; then
  echo ""
  echo "Working on registry-images without branches"
  project_registry_loop registry_repo_cleanup && echo "Finished successfully" || echo "ERROR cleaning registry"
else
  echo "NOT cleaning registry..."
fi

if [ "x${REGISTRY_LIST}" == "xtrue" ]; then
  echo ""
  echo "Listing all registry-images"
  project_registry_loop registry_repo_list && echo "Finished successfully" || echo "ERROR listing registry"
fi

if [ -n "${JIRA_URL}" ]; then
  echo ""
  echo "Setting Jira-Integration"
  integration_jira && echo "Finished successfully" || echo "ERROR listing registry"
fi


if [ -n "${PIPELINE_CHECK}" ]; then
  echo ""
  echo "Checking Pipelines"
  pipeline_check && echo "Finished successfully" || echo "ERROR checking pipelines"
fi

#!/bin/bash

project_registry_loop(){
  for REPO_ID in ${PROJECTS} ;do

    # display repo-path + check if registry is enabled for project
    REPO_PATH="$(curl -L --silent --header "PRIVATE-TOKEN: $TOKEN" "$URL/projects/$REPO_ID" | jq -r '.path_with_namespace')"
    REPO_REGISTRY_ENABLED="$(curl -L --silent --header "PRIVATE-TOKEN: $TOKEN" "$URL/projects/$REPO_ID" | jq -r '.container_registry_enabled')"

    # if we have a registry enabled, get branches + registry-repositories
    if ${REPO_REGISTRY_ENABLED}; then
      REPO_BRANCHES="$(pagination "/projects/$REPO_ID/repository/branches" | jq -r '.[].name'| tr '/' '-'|tr '.' '-' )"
      REPO_REGISTRY_REPOS="$(pagination "projects/$REPO_ID/registry/repositories" | jq -r '.[].id')"

      $1
    # if no registry is enabled in this project
    # print info
    else
      echo -en "\nid: ${REPO_ID}, registry-id: DISABLED, path: ${REPO_PATH}"
    fi
      echo ""
  done
}


registry_repo_list(){
  # display info
  echo -ne "\n\n"
  echo "id: ${REPO_ID}, ${REPO_PATH}, registry-id: ${REPO_REGISTRY_ENABLED}"
  echo -ne "  - images:"

  # iterate over registry-repos + get the image-tags
  # for every image-tag check if there
  # is a corresponding branch-name in this project
  for id in $REPO_REGISTRY_REPOS; do

    REPO_REGISTRY_PATHS="$(pagination "/projects/$REPO_ID/registry/repositories/$id/tags" | jq -r '.[].path')"

    for tag in $REPO_REGISTRY_PATHS; do

      echo -ne "\n    /$tag "
    done
  done
}



registry_repo_cleanup(){
  # display info
  echo -ne "\n\n"
  echo "id: ${REPO_ID}, ${REPO_PATH}, registry-id: ${REPO_REGISTRY_ENABLED}"
  echo "  - branches:"
  for branch in $REPO_BRANCHES; do
    echo "    - $branch"
  done
  echo ""
  echo "  - images:"

  # iterate over registry-repos + get the image-tags
  # for every image-tag check if there
  # is a corresponding branch-name in this project
  for id in $REPO_REGISTRY_REPOS; do

    echo "    - $id:"
    # # special case master tags
    # # keep last 10
    # curl -L --silent --header "PRIVATE-TOKEN: $TOKEN" -X DELETE \
    #   --data 'name_regex=master-.*' \
    #   --data 'older_than=1month' \
    #   "$URL/projects/$REPO_ID/registry/repositories/$id/tags"

    # curl -L --silent --header "PRIVATE-TOKEN: $TOKEN" -X DELETE \
    #   --data 'name_regex=master-.*' \
    #   --data 'older_than=1month' \
    #   "$URL/projects/$REPO_ID/registry/repositories/$id/tags"

    REPO_REGISTRY_TAGS="$(curl -L --silent --header "PRIVATE-TOKEN: $TOKEN" "$URL/projects/$REPO_ID/registry/repositories/$id/tags" | jq -r '.[].name' )"

    for tag in $REPO_REGISTRY_TAGS; do
      # check if tag has a corresponding branch-name
      # and mark for deletion (exit-code 1)

      containsElement "${tag}" ${REPO_BRANCHES}
      MARKED="$?"
      # echo "tag: $tag , branches: $branches"

      # if tag is in Exclude-List unmark it
      containsElement "${tag}" $REPO_REGISTRY_TAGS_EXCLUDES \
      && MARKED="0"

      # echo -ne "\n    - $id/$tag "

      # if marked for deletion, print info in dry-run
      # or delete tag
      if [[ "$tag" == "latest" ]] || [[ "$MARKED" == "0" ]]; then

        echo "      - I   | $id/$tag"

      elif [ "$MARKED" == "1" ] ; then


        if $DRY_RUN; then
          echo "      - D   | $id/$tag"
        else
          echo -ne "      - D   | $id/$tag"
          # really run curl
          curl -L --silent --header "PRIVATE-TOKEN: $TOKEN" \
            -X DELETE "$URL/projects/$REPO_ID/registry/repositories/$id/tags/$tag"
          echo " --> GONE!"
        fi
      fi
    done

    # handling master as special case
    echo "      - D>1m| $id/master-* (keeping last 10)"
    if ! $DRY_RUN; then
      # really run curl
      curl -L --silent --header "PRIVATE-TOKEN: $TOKEN" \
        -X DELETE \
        --data 'keep_n=10' \
        --data "older_than=1month" \
        --data "name_regex=master-*" \
        "$URL/projects/$REPO_ID/registry/repositories/$id/tags"

      echo " --> GONE!"
    fi

  done
}


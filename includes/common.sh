#!/bin/bash

DRY_RUN="true"
# shellcheck disable=SC2034  # TODO: still not sure where to put it
REPO_REGISTRY_TAGS_EXCLUDES="latest master"

help(){
  cat << EOF
  ./gl_cleanup.sh -u=https://\$gitlabdomain/api/v4 -t=\$TOKEN [-d] [-p=\$project-id] [-r|-b]

  parameter:
    Limits:
      -p=\$project-id                  (default: all projects)
      -g=\$group                       limit operations to specific group e.g. 'sys%2fmirror' ( req. for registry-cleanup!)

    Operations:
      -p|--pipeline                   check for valid pipelines
      -c|--clone                      clone specified project/s or all
      -b|--branches                   delete already merged branches
      -r|--registry                   cleanup registry
      -rl|--registry-list             list registry-images
      -j=|--jira-url=                 Jira-Url
      -ju=|--jira-user=               Jira-User
      -jt=|--jira-token=              Jira-Password/Token
      -jtid=|--jira-transition-id=    Jira-Transition-ID

    Others:
      -t|--token                      user-token for Gitlab-api
      -u|--url                        Gitlab-URL (e.g.: https://git.chefkoch.net/api/v4)
      -d|--delete                     really delete container-tags (default: dry-run)
      -h|--help                       This ;P

  requirements:
    - jq
    - curl
EOF
}


for i in "$@"; do
  case $i in
    -p|--pipeline)
      PIPELINE_CHECK="true"
      ;;
    -c|--clone)
      # shellcheck disable=SC2034  # used in downstream script
      CLONE="true"
      ;;
    -r|--registry)
      # shellcheck disable=SC2034  # used in downstream script
      REGISTRY="true"
      ;;
    -b|--branches)
      # shellcheck disable=SC2034  # used in downstream script
      BRANCH="true"
      ;;
    -t=*|--token=*)
      # shellcheck disable=SC2034  # used in downstream script
      TOKEN="${i#*=}"
      ;;
    -u=*|--url=*)
      # shellcheck disable=SC2034  # used in downstream script
      URL="${i#*=}"
      ;;
    -d|--delete)
      # shellcheck disable=SC2034  # used in downstream script
      DRY_RUN="false"
      ;;
    -p=*|--project=*)
      PROJECTS="${i#*=}"
      echo "Limiting to: project/$PROJECTS"
      ;;
    -g=*|--group=*)
      GROUP="groups/${i#*=}"
      echo "Limiting to: $GROUP"
      ;;
    -rl|--list-images)
      # shellcheck disable=SC2034  # used in downstream script
      REGISTRY_LIST="true"
      ;;
    -j=*|--jira-url=*)
      # shellcheck disable=SC2034  # used in downstream script
      JIRA_URL="${i#*=}"
      ;;
    -ju=*|--jira-user=*)
      # shellcheck disable=SC2034  # used in downstream script
      JIRA_USER="${i#*=}"
      ;;
    -jt=*|--jira-token=*)
      # shellcheck disable=SC2034  # used in downstream script
      JIRA_TOKEN="${i#*=}"
      ;;
    -jtid=*|--jira-transition-id=*)
      # shellcheck disable=SC2034  # used in downstream script
      JIRA_TID="${i#*=}"
      ;;

    -h|--help)
      help
      exit 0
      ;;
    *);;
  esac
done

#[ -z "$1" ] && help && exit 0

! command -v jq >/dev/null 2>&1 && echo -e "\n ERROR: jq missing\n" && exit 1
[ -z "$TOKEN" ] && echo -e "\n ERROR: TOKEN missing\n" && exit 1
[ -z "$URL" ] && echo -e "\n ERROR: URL missing\n" && exit 1

# checks if $2 contains $1
containsElement () {
  local e match="$1"
  shift
  for e; do
     echo $match |grep -qi ^$e \
    && return 0
  done
  return 1
}



pagination(){
  last_page="$(curl -s -I --header "PRIVATE-TOKEN: $TOKEN" "$URL/${1}?per_page=100" | grep '^link:' | sed -e 's/^link:.*&page=//g' |sed 's/&.*//g')"
  # echo "# $last_page" >> /tmp/test
  test -n "$last_page" || last_page="1"
  for page in $(seq 1 "$last_page"); do
    RETURN="$RETURN $(curl --silent --header "PRIVATE-TOKEN: $TOKEN" "$URL/${1}?per_page=100&page=${page}")"
  done

  echo "$RETURN"
}


# get list of projects
#echo "$URL/$GROUP/projects"

[ -n "$PROJECTS" ] || PROJECTS="$(pagination "$GROUP/projects"| jq '.[].id' |sort -n)"
[ -n "$GROUP" ] && SUBGROUP_IDS="$(pagination "/$GROUP/subgroups" | jq -r '.[].id' | sort -n)"

# echo -ne "PROJECTS: \n $PROJECTS\n"
# echo -ne "GROUPS: $SUBGROUP_IDS\n"


## TODO: NOT RECURSIVE YET
## ONLY /group/subgroup supported so far

echo  "Subgroups:"
for subgroup_id in ${SUBGROUP_IDS}; do

  subgroup_name="$(pagination "/groups/$subgroup_id" | jq -r '.name')"
  echo "  - id: $subgroup_id, name: ${subgroup_name}"


  PROJECTS="$PROJECTS $(pagination "/groups/$subgroup_id/projects" | jq -r '.[].id')"
done


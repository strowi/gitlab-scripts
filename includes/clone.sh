#!/bin/bash

repos_clone(){

  for REPO_ID in ${PROJECTS} ;do

    #echo "REPO_ID"
    REPO_PATH="$(/usr/bin/curl -L --silent --header "PRIVATE-TOKEN: $TOKEN" "$URL/projects/$REPO_ID" | /usr/bin/jq '.path_with_namespace'|sed 's/"//g')"
    REPO_URL="$(/usr/bin/curl -L --silent --header "PRIVATE-TOKEN: $TOKEN" "$URL/projects/$REPO_ID" | /usr/bin/jq '.ssh_url_to_repo')"

    # mkdir -p $(dirname $REPO_PATH)

    echo "= cloning into $REPO_PATH ($REPO_URL)"
    bash -c "git clone --no-checkout --quiet $REPO_URL $PWD/$REPO_PATH"

  #  sed "s#XXXPATHXXX#${PWD}\/${REPO_PATH}#g" /home/rvg/src/template.sublime-project > /tmp/$(echo $REPO_PATH|sed 's/\//_/g').sublime-project
  done
}


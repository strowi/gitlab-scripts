# curl --header "Content-Type: application/json" "https://gitlab.example.com/api/v4/projects/:id/ci/lint" --data '{"content": "{ \"image\": \"ruby:2.6\", \"services\": [\"postgres\"], \"before_script\": [\"bundle install\", \"bundle exec rake db:create\"], \"variables\": {\"DB_NAME\": \"postgres\"}, \"types\": [\"test\", \"deploy\", \"notify\"], \"rspec\": { \"script\": \"rake spec\", \"tags\": [\"ruby\", \"postgres\"], \"only\": [\"branches\"]}}"}'

#!/bin/bash

pipeline_check(){

  for REPO_ID in ${PROJECTS} ;do

    REPO_PATH="$(curl -L --silent --header "PRIVATE-TOKEN: $TOKEN" "$URL/projects/$REPO_ID" | jq -r '.path_with_namespace')"

    echo -ne "\nid: ${REPO_ID}, ${REPO_PATH}: "
    curl -L --silent --header "PRIVATE-TOKEN: $TOKEN" "$URL/projects/$REPO_ID/ci/lint?include_jobs=false" | jq -r '. | "\(.valid) \n \(.errors)"'


  done
}
